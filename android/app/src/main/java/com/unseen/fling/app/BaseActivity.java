package com.unseen.fling.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import com.unseen.fling.utils.CrashHandler;

public abstract class BaseActivity extends ActionBarActivity implements FragmentManager.OnBackStackChangedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().addOnBackStackChangedListener(this);
    }

    /**
     * Launch a new activty with default application flags
     *
     * @param clz
     */
    public void launchActivity(Class<?> clz) {
        launchActivity(clz, null);
    }

    /**
     * Launch a new activty with default application flags
     *
     * @param clz
     * @param extras
     */
    public void launchActivity(Class<?> clz, Bundle extras) {
        Intent intent = new Intent(BaseActivity.this, clz);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        if (extras != null) {
            intent.putExtras(extras);
        }
        startActivity(intent);
    }

    /**
     * Hides soft keyboard from screen.
     *
     * @param v
     */
    public void hideKeyboard(View v) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public void startFragment(int containerId, Fragment fragment) {
        startFragment(containerId, fragment, false);
    }

    public void startFragment(int containerId, Fragment fragment, boolean addToBackStask) {
        if (fragment == null) {
            CrashHandler.logException(new IllegalArgumentException("Fragment should not be null !"));
            return;
        }
        final FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(containerId, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        if (addToBackStask) {
            ft.addToBackStack(fragment.getClass().getSimpleName());
        } else {
            //            ft.addToBackStack(null);
        }
        ft.commit();
    }

    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0) {
            finish();
        }
    }

}
