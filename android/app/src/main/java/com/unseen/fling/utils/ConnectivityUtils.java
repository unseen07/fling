package com.unseen.fling.utils;

import android.content.Context;
import android.net.ConnectivityManager;

public class ConnectivityUtils {

    private ConnectivityUtils() {
        throw new RuntimeException("This is a utils class.");
    }

    private static ConnectivityManager getManager(Context context) {
        return (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    }

    public static boolean isWifiConnected(Context context) {
        return getManager(context).getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
    }

    public static boolean isMobileConnected(Context context) {
        final ConnectivityManager connectivityManager = getManager(context);
        return ((connectivityManager.getNetworkInfo(
                ConnectivityManager.TYPE_MOBILE) == null ? false : connectivityManager.getNetworkInfo(
                ConnectivityManager.TYPE_MOBILE).isConnected()));
    }

    public static boolean isConnected(Context context) {
        return isMobileConnected(context) || isWifiConnected(context);
    }

}
    
	
	

