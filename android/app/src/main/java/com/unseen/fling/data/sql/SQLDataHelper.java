package com.unseen.fling.data.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.unseen.fling.ui.utils.ResourceUtils;

public class SQLDataHelper extends SQLiteOpenHelper {

    private static final String CREATE_TABLE_FLING_PATH = "schema/create_fling_table.sql";

    private final static int DB_VERSION = 1;
    public final static String DB_NAME = "fling.db";
    private static final String TAG = "SQLDataHelper";

    private final Context mContext;

    public SQLDataHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.beginTransaction();
            // We create the tables
            final String createFlingTable = ResourceUtils.getAssetsAsString(mContext, CREATE_TABLE_FLING_PATH);
            db.execSQL(createFlingTable);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            Log.e(TAG, "Error creating DB " + DB_NAME, e);
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
