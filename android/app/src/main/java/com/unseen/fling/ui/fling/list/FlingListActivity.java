package com.unseen.fling.ui.fling.list;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import com.unseen.fling.R;
import com.unseen.fling.app.BaseActivity;

public class FlingListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fling_list);

        if (savedInstanceState == null) {
            Fragment newFragment = FlingListFragment.newInstance();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.container, newFragment).commit();
        }
    }

}
