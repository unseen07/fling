package com.unseen.fling.ui.utils;

import android.content.Context;

import java.io.*;

/**
 * Utility class to manage Resources
 */
public final class ResourceUtils {

    private static final int DEFAULT_BUFFER_SIZE = 2048;
    private static final String DEFAULT_ENCODING = "UTF8";

    /*
     * Private constructor
     */
    private ResourceUtils() {
        throw new AssertionError("Never call this!!!");
    }

    public static String getAssetsAsString(Context context,
                                           String assetsFile) throws IOException {
        InputStream is = context.getAssets().open(assetsFile);
        String result = toString(is, DEFAULT_ENCODING);
        return result;
    }

    public static String toString(final InputStream in, final String encoding)
            throws IOException {
        InputStreamReader reader = new InputStreamReader(in, encoding);
        StringWriter writer = new StringWriter();
        copy(reader, writer, DEFAULT_BUFFER_SIZE);
        return writer.getBuffer().toString();
    }

    public static void copy(final Reader in, final Writer out, final int bufferSize)
            throws IOException {
        char[] buffer = new char[bufferSize];
        int read = 0;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }
}
