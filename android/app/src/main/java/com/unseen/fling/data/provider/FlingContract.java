package com.unseen.fling.data.provider;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import com.unseen.fling.model.Fling;

import java.util.ArrayList;
import java.util.List;

public class FlingContract {

    private static final String AUTHORITY = FlingContentProvider.AUTHORITY;

    public static class Flings implements BaseColumns {

        public static final String TABLE_NAME = "fling";

        static final String FLING_PATH = "fling";
        static final String REPORT_PATH = FLING_PATH + "/report";

        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + FLING_PATH);
        public static final Uri CONTENT_REPORT_URI = Uri.parse("content://" + AUTHORITY + "/" + REPORT_PATH);

        public static ContentValues toContentValues(Fling fling) {
            ContentValues values = new ContentValues();
            values.put(Columns.ID, fling.getId());
            values.put(Columns.IMAGE_ID, fling.getImageId());
            values.put(Columns.USER_ID, fling.getUserId());
            values.put(Columns.TITLE, fling.getTitle());
            values.put(Columns.USERNAME, fling.getUsername());
            return values;
        }

        public static List<Fling> fromCursor(Cursor c) {
            List<Fling> flings = new ArrayList<Fling>(c.getCount());

            while (c != null && c.moveToNext()) {
                Fling.Builder builder = new Fling.Builder();
                builder.setId(c.getInt(c.getColumnIndex(Columns.ID)));
                builder.setImageId(c.getInt(c.getColumnIndex(Columns.IMAGE_ID)));
                builder.setTitle(c.getString(c.getColumnIndex(Columns.TITLE)));
                builder.setUserId(c.getInt(c.getColumnIndex(Columns.USER_ID)));
                builder.setUsername(c.getString(c.getColumnIndex(Columns.USERNAME)));
                flings.add(builder.build());
            }
            return flings;
        }

        public interface Columns {
            String ID = BaseColumns._ID;
            String IMAGE_ID = "image_id";
            String USER_ID = "user_id";
            String TITLE = "title";
            String USERNAME = "username";
            String WIDTH = "width";
            String IMAGE_SIZE = "image_size";
            String NUM_POST = "num_post";
        }
    }
}
