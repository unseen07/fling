package com.unseen.fling.model;

import com.google.gson.annotations.SerializedName;

public class Fling {

    @SerializedName("ID")
    private int mId;

    @SerializedName("ImageID")
    private int mImageId;

    @SerializedName("Title")
    private String mTitle;

    @SerializedName("UserID")
    private int mUserId;

    @SerializedName("UserName")
    private String mUsername;

    public Fling() {
    }

    public Fling(int id, int imageId, String title, int userId, String username) {
        this.mId = id;
        this.mImageId = imageId;
        this.mTitle = title;
        this.mUserId = userId;
        this.mUsername = username;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getImageId() {
        return mImageId;
    }

    public void setImageId(int imageId) {
        mImageId = imageId;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }


    public static class Builder {

        private int mId;
        private int mImageId;
        private String mTitle;
        private int mUserId;
        private String mUsername;


        public Fling build() {
            return new Fling(mId, mImageId, mTitle, mUserId, mUsername);
        }

        public Builder setId(int id) {
            mId = id;
            return this;
        }

        public Builder setImageId(int imageId) {
            mImageId = imageId;
            return this;
        }

        public Builder setTitle(String title) {
            mTitle = title;
            return this;
        }

        public Builder setUserId(int userId) {
            mUserId = userId;
            return this;
        }

        public Builder setUsername(String username) {
            mUsername = username;
            return this;
        }
    }
}
