package com.unseen.fling;

import android.app.Application;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.PicassoTools;

/**
 *
 */
public class FlingApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        PicassoTools.clearCache(Picasso.with(this));
    }
}
