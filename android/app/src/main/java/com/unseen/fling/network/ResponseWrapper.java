package com.unseen.fling.network;

/**
 * Wrapper class holding the response from any Loader to have a common way of getting the data.
 */
public class ResponseWrapper<T> {

    // This is supposed to be custom application specific error codes ... :)
    public final static int STATUS_OK = 200;
    public final static int STATUS_NO_DATA = 404;

    private final int code;
    private final String message;
    private final T data;

    public ResponseWrapper(int code, String message, T data) {
        super();
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getData() {
        return data;
    }
}
