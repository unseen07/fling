package com.unseen.fling.model;

public class PhotoStats {

    private int mId;
    private int mImageId;
    private int mUserId;
    private String mUsername;
    private int mSize;
    private int mWidth;

    public PhotoStats() {
    }

    public PhotoStats(int id, int imageId, int userId, String username, int size, int width) {
        mId = id;
        mImageId = imageId;
        mUserId = userId;
        mUsername = username;
        mSize = size;
        mWidth = width;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getImageId() {
        return mImageId;
    }

    public void setImageId(int imageId) {
        mImageId = imageId;
    }

    public int getSize() {
        return mSize;
    }

    public int getWidth() {
        return mWidth;
    }

    public int getUserId() {
        return mUserId;
    }

    public void setUserId(int userId) {
        mUserId = userId;
    }

    public String getUsername() {
        return mUsername;
    }

    public void setUsername(String username) {
        mUsername = username;
    }


    public static class Builder {

        private int mId;
        private int mImageId;
        private int mUserId;
        private String mUsername;
        private int mSize;
        private int mWidth;


        public PhotoStats build() {
            return new PhotoStats(mId, mImageId, mUserId, mUsername, mSize, mWidth);
        }

        public Builder setId(int id) {
            mId = id;
            return this;
        }

        public Builder setImageId(int imageId) {
            mImageId = imageId;
            return this;
        }

        public Builder setUserId(int userId) {
            mUserId = userId;
            return this;
        }

        public Builder setUsername(String username) {
            mUsername = username;
            return this;
        }

        public Builder setSize(int size) {
            mSize = size;
            return this;
        }

        public Builder setWidth(int width) {
            mWidth = width;
            return this;
        }
    }
}
