package com.unseen.fling.stats;

import android.app.IntentService;
import android.content.ContentProviderOperation;
import android.content.ContentValues;
import android.content.Intent;
import android.util.Log;
import com.unseen.fling.data.provider.FlingContract;

public class StatsService extends IntentService {

    private static final String TAG = StatsService.class.getSimpleName();

    public static final String ACTION_SAVE_IMAGE_INFO = "fling.intent.action.ACTION_SAVE_IMAGE_INFO";

    public StatsService() {
        super("StatsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        Log.d(TAG, "onHandleIntent: " + action);
        if (ACTION_SAVE_IMAGE_INFO.equals(action)) {
            saveImageInfoData(intent);
        }
    }

    private void saveImageInfoData(Intent intent) {
        int id = intent.getIntExtra(FlingContract.Flings.Columns.ID, -1);
        if (id > -1) {
            ContentValues cv = new ContentValues();
            cv.put(FlingContract.Flings._ID, id);
            cv.put(FlingContract.Flings.Columns.IMAGE_SIZE, intent.getIntExtra(
                    FlingContract.Flings.Columns.IMAGE_SIZE, 0));
            cv.put(FlingContract.Flings.Columns.WIDTH, intent.getIntExtra(
                    FlingContract.Flings.Columns.WIDTH, 0));

            ContentProviderOperation.newUpdate(FlingContract.Flings.CONTENT_URI)
                    .withSelection(FlingContract.Flings._ID, new String[]{String.valueOf(id)})
                    .withValues(cv)
                    .withYieldAllowed(true)
                    .build();
            int res = getApplicationContext().getContentResolver().update(FlingContract.Flings.CONTENT_URI, cv, "_id = " + id, null);
            System.out.println(">>>" + res + " xx " + cv);
        }else {
            System.out.println(">>> error saving info");
        }
    }

}
