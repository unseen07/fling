package com.unseen.fling.ui.fling.list;

import com.unseen.fling.model.Fling;
import com.unseen.fling.mvp.Scene;

import java.util.List;

public interface FlingListScene extends Scene {

    void showFlingList(List<Fling> flingList);
    void showError(int errorMsgId);
    void showLoading();

}
