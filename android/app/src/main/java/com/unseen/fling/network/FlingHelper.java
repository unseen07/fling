package com.unseen.fling.network;

import android.content.Context;
import android.util.Log;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.unseen.fling.R;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 *
 */
public class FlingHelper {

    private static final String TAG = "FlingHelper";

    private static final String ENDPOINT = "http://challenge.superfling.com";
    public static final String PHOTO_ENDPOINT = ENDPOINT + "/photos/";

    public RestAdapter getRestAdapter(Context context) {

        return new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setLog(new RestAdapter.Log() {
                    @Override
                    public void log(String message) {
                        Log.d(TAG, message);
                    }
                })
                .build();
    }

}
