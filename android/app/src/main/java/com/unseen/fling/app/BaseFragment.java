package com.unseen.fling.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.ViewStub;
import com.unseen.fling.mvp.Presenter;
import com.unseen.fling.ui.utils.UIUtils;

/**
 * *
 *
 * @param <T>
 */
public abstract class BaseFragment<T extends Presenter> extends Fragment {

    protected T mPresenter;

    protected View mLoadingView;
    protected View mContainerView;

    private BaseFragment mCurrentFragment;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (mPresenter != null) {
            mPresenter.initActivity(getBaseActivity());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mPresenter != null) {
            mPresenter.onStart();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter != null) {
            mPresenter.onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mPresenter != null) {
            mPresenter.onPause();
        }
    }

    protected void showLoadingView(boolean show) {
        if (mLoadingView != null) {
            UIUtils.resetLoadingView(mLoadingView);
            mLoadingView.setVisibility(show ? View.VISIBLE : View.GONE);
        } else {
            Log.e("XX", "loading view is null");
        }

        if (mContainerView != null) {
            mContainerView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    protected void showErrorMessage(String message) {
        UIUtils.showErrorMessage(mLoadingView, message);
        if (mContainerView != null) {
            mContainerView.setVisibility(View.GONE);
        }
    }

    protected BaseActivity getBaseActivity() {
        return ((BaseActivity) getActivity());
    }

    protected final View findView(View parentView, int viewId, int stubId) {
        View view = parentView.findViewById(viewId);
        if (view == null) {
            if (stubId > 0) {
                final ViewStub stub = (ViewStub) parentView.findViewById(stubId);
                if (stub != null) {
                    view = stub.inflate();
                }
            }
        }
        return view;
    }

    public final void initPresenter(T presenter) {
        mPresenter = presenter;
    }

}
