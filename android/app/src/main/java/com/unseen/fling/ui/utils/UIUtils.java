package com.unseen.fling.ui.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import com.unseen.fling.R;

public final class UIUtils {

    private UIUtils() {
        throw new RuntimeException("Can't instantiate a utility class");
    }

    /**
     * This method returns the reference of the View with the given Id in the
     * layout of the Activity passed as parameter
     *
     * @param act    The Activity that is using the layout with the given View
     * @param viewId The id of the View we want to get a reference
     * @return The View with the given id and type
     */
    public static <T extends View> T findViewById(Activity act, int viewId) {
        View containerView = act.getWindow().getDecorView();
        return findViewById(containerView, viewId);
    }

    /**
     * This method returns the reference of the View with the given Id in the
     * view passed as parameter
     *
     * @param containerView The container View
     * @param viewId        The id of the View we want to get a reference
     * @return The View with the given id and type
     */
    @SuppressWarnings("unchecked")
    public static <T extends View> T findViewById(View containerView, int viewId) {
        View foundView = containerView.findViewById(viewId);
        return (T) foundView;
    }

    public static void showErrorMessage(View loadingView, String message) {
        loadingView.findViewById(R.id.pb_loading_bar).setVisibility(View.INVISIBLE);
        ((TextView) loadingView.findViewById(R.id.tv_loading_text)).setText(message);
    }

    public static void resetLoadingView(View loadingView) {
        loadingView.findViewById(R.id.pb_loading_bar).setVisibility(View.VISIBLE);
        ((TextView) loadingView.findViewById(R.id.tv_loading_text)).setText(R.string.loading);
    }

    public static void showLoadingView(boolean show, View loadingView, View containerview) {
        if (loadingView != null) {
            UIUtils.resetLoadingView(loadingView);
            loadingView.setVisibility(show ? View.VISIBLE : View.GONE);
        } else {
            Log.e("XX", "loading view is null");
        }

        if (containerview != null) {
            containerview.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    public static int getScreenWidth(Context context) {
        return context.getResources().getDisplayMetrics().widthPixels;
    }

    public static int getDimenInPixel(Context context, int targetResId) {
        Resources resources = context.getResources();
        return resources.getDimensionPixelSize(targetResId);
    }
}
