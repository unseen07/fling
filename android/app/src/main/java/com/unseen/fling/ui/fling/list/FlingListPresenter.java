package com.unseen.fling.ui.fling.list;

import android.app.Activity;
import android.app.LoaderManager;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import com.unseen.fling.R;
import com.unseen.fling.data.loader.FlingLoader;
import com.unseen.fling.data.loader.LoaderUtils;
import com.unseen.fling.data.provider.FlingContract;
import com.unseen.fling.model.Fling;
import com.unseen.fling.mvp.Presenter;
import com.unseen.fling.network.ResponseWrapper;
import com.unseen.fling.utils.ConnectivityUtils;

import java.util.List;

public class FlingListPresenter extends Presenter<FlingListScene> implements LoaderManager.LoaderCallbacks<ResponseWrapper<List<Fling>>> {

    protected FlingListPresenter(FlingListScene scene) {
        super(scene);
    }

    @Override
    public void onStart() {
        if (ConnectivityUtils.isConnected(getActivity())) {
            loadData();
        } else {
            Toast.makeText(getActivity(), R.string.movie_list_no_connection, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onPause() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void loadData() {
        final Activity activity = getActivity();
        if (activity != null && ConnectivityUtils.isConnected(getActivity())) {
            Bundle bundle = new Bundle();
            activity.getLoaderManager().restartLoader(LoaderUtils.LOADER_MOVIE_POPULAR_ID, bundle, this);
            mScene.showLoading();
        }
    }

    @Override
    public Loader<ResponseWrapper<List<Fling>>> onCreateLoader(int id, Bundle args) {
        return new FlingLoader(getActivity(), args);
    }

    @Override
    public void onLoadFinished(Loader<ResponseWrapper<List<Fling>>> loader,
                               ResponseWrapper<List<Fling>> data) {
        if (data.getCode() == ResponseWrapper.STATUS_OK) {
            mScene.showFlingList(data.getData());
        } else {
            mScene.showError(R.string.no_data);
        }
    }

    @Override
    public void onLoaderReset(Loader<ResponseWrapper<List<Fling>>> loader) {

    }

    public void printReport() {
        Cursor c = getActivity().getContentResolver().query(FlingContract.Flings.CONTENT_REPORT_URI, null, null, null,
                                                            null);

        StringBuilder sb = new StringBuilder();
        sb.append("Username");
        sb.append("\t");
        sb.append("Num of Posts");
        sb.append("\t");
        sb.append("Avg Image Size");
        sb.append("\t");
        sb.append("Greatest Width");
        sb.append("\t");

        Log.d("report", sb.toString() + " " + c.getCount());

        while (c != null && c.moveToNext()) {
            sb = new StringBuilder();
            sb.append(c.getString(c.getColumnIndex(FlingContract.Flings.Columns.USERNAME)));
            sb.append("\t");
            sb.append(c.getInt(c.getColumnIndex(FlingContract.Flings.Columns.NUM_POST)));
            sb.append("\t");
            sb.append(c.getInt(c.getColumnIndex(FlingContract.Flings.Columns.IMAGE_SIZE)));
            sb.append("\t");
            sb.append(c.getInt(c.getColumnIndex(FlingContract.Flings.Columns.WIDTH)));
            sb.append("\t");

            Log.d("report", sb.toString());
        }

    }
}
