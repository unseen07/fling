package com.unseen.fling.network;

import com.unseen.fling.model.Fling;
import retrofit.Callback;
import retrofit.http.GET;

import java.util.List;

public interface FlingService {

    @GET("/")
    List<Fling> getFlingList();

}
