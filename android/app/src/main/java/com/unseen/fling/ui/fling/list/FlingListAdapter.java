package com.unseen.fling.ui.fling.list;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.unseen.fling.R;
import com.unseen.fling.model.Fling;
import com.unseen.fling.network.FlingHelper;
import com.unseen.fling.ui.utils.CropTransformation;
import com.unseen.fling.ui.utils.UIUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FlingListAdapter extends RecyclerView.Adapter<FlingListAdapter.FlingViewHolder> {

    private FlingItemCallback mItemCallback;

    private List<Fling> mFlings;
    private List<Target> mTargets;
    private Set<Long> mAnimatedItems = new HashSet<Long>();

    public FlingListAdapter(List<Fling> results) {
        mFlings = results;
        mTargets = new ArrayList<Target>();
    }

    @Override
    public FlingViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_fling, parent, false);
        return new FlingViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final FlingViewHolder holder, int position) {
        final Context context = holder.itemView.getContext();
        final Fling fling = mFlings.get(position);

        holder.title.setText(String.valueOf(fling.getTitle()));
        loadImage(context, fling, FlingHelper.PHOTO_ENDPOINT + fling.getImageId(), holder.backdrop);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
                                               @Override
                                               public void onClick(View v) {
                                                   if (mItemCallback != null) {
                                                       mItemCallback.onFlingClick(fling);
                                                   }
                                                   doFunkyAnumation(v);
                                               }
                                           }
        );
    }

    private void doFunkyAnumation(View v) {
        ObjectAnimator animation = ObjectAnimator.ofFloat(v, "rotationX", 0.0f, 360f);
        animation.setDuration(1800);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.start();
    }

    private void setAnimation(View viewToAnimate, long id) {
        if (!mAnimatedItems.contains(id)) {

            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), R.anim.scale_up_fade_in);
            viewToAnimate.startAnimation(animation);
            mAnimatedItems.add(id);
        }
    }

    private Target loadImage(final Context context, final Fling fling, final String url, final ImageView imageView) {
        imageView.setTag(url);
        final Target target = new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                Log.d("", "image loaded " + url + bitmap.getByteCount() + " : ");
                setAnimation(imageView, fling.getId());
                handleLoadedBitmap(bitmap, url, imageView);
                mTargets.remove(this);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {
                handleDrawable(errorDrawable, url, imageView);
                mTargets.remove(this);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {
                handleDrawable(placeHolderDrawable, url, imageView);
            }
        };
        Picasso.with(context)
                .load(url)
                .transform(new CropTransformation(context, fling.getId(), UIUtils.getScreenWidth(context),
                                                  UIUtils.getDimenInPixel(context, R.dimen.image_list_height)))
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.error_placeholder)
                .into(target);
        mTargets.add(target);
        return target;
    }

    public void handleDrawable(final Drawable drawable, final String url, final ImageView imageView) {
        if (!TextUtils.isEmpty(url) && url.equals(imageView.getTag())) {
            imageView.setImageDrawable(drawable);
        }
    }

    public void handleLoadedBitmap(final Bitmap b, final String url, final ImageView imageView) {
        if (!TextUtils.isEmpty(url) && url.equals(imageView.getTag())) {
            imageView.setImageBitmap(b);
        } else {
            // we don't need it anymore
            b.recycle();
        }
    }

    @Override
    public int getItemCount() {
        if (mFlings != null) {
            return mFlings.size();
        }
        return 0;
    }

    public void setItemCallback(FlingItemCallback itemCallback) {
        mItemCallback = itemCallback;
    }

    public interface FlingItemCallback {

        void onFlingClick(Fling movie);
    }

    public static class FlingViewHolder extends RecyclerView.ViewHolder {

        final TextView title;
        final ImageView backdrop;

        public FlingViewHolder(View itemView) {
            super(itemView);
            title = UIUtils.findViewById(itemView, R.id.tv_fling_title);
            backdrop = UIUtils.findViewById(itemView, R.id.iv_backdrop);
        }
    }
}
