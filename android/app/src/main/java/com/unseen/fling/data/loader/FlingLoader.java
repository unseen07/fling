package com.unseen.fling.data.loader;

import android.content.AsyncTaskLoader;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.os.Bundle;
import android.os.RemoteException;
import com.unseen.fling.data.provider.FlingContentProvider;
import com.unseen.fling.data.provider.FlingContract;
import com.unseen.fling.model.Fling;
import com.unseen.fling.network.FlingHelper;
import com.unseen.fling.network.FlingService;
import com.unseen.fling.network.ResponseWrapper;
import com.unseen.fling.utils.ConnectivityUtils;
import com.unseen.fling.utils.CrashHandler;

import java.util.ArrayList;
import java.util.List;

public class FlingLoader extends AsyncTaskLoader<ResponseWrapper<List<Fling>>> {

    private ResponseWrapper<List<Fling>> mResponse;

    public FlingLoader(Context context, Bundle bundle) {
        super(context);
    }

    @Override
    public ResponseWrapper<List<Fling>> loadInBackground() {
        List<Fling> flingList;

        // get the data from the network
        if (ConnectivityUtils.isConnected(getContext())) {
            flingList = getDataFromNetwork(getContext());
        } else {
            flingList = getDataFromDevice();
        }

        if (flingList == null) {
            mResponse = new ResponseWrapper<List<Fling>>(ResponseWrapper.STATUS_NO_DATA, "no data", null);
        } else {
            mResponse = new ResponseWrapper<List<Fling>>(ResponseWrapper.STATUS_OK, "success", flingList);
        }

        return mResponse;
    }

    private List<Fling> getDataFromNetwork(Context context) {
        FlingHelper helper = new FlingHelper();
        FlingService service = helper.getRestAdapter(context).create(FlingService.class);
        List<Fling> flingList = service.getFlingList();

        // saving it to disk now
        if (flingList != null) {
            try {
                saveDataToDevice(flingList);
            } catch (RemoteException | OperationApplicationException e) {
                CrashHandler.logException(e);
            }
        }

        return flingList;
    }

    private List<Fling> getDataFromDevice() {
        Cursor c = getContext().getContentResolver().query(FlingContract.Flings.CONTENT_URI, null, null, null, null);
        return FlingContract.Flings.fromCursor(c);
    }

    private void saveDataToDevice(List<Fling> flingList) throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> operations = new ArrayList<ContentProviderOperation>();

        for (Fling fling : flingList) {
            operations.add(
                    ContentProviderOperation.newInsert(FlingContract.Flings.CONTENT_URI)
                            .withValues(FlingContract.Flings.toContentValues(fling))
                            .withYieldAllowed(true)
                            .build()
            );
        }

        getContext().getContentResolver().applyBatch(FlingContentProvider.AUTHORITY, operations);
    }

    @Override
    protected void onStartLoading() {
        if (mResponse != null && mResponse.getCode() == ResponseWrapper.STATUS_OK) {
            deliverResult(mResponse);
        } else if (takeContentChanged() || mResponse == null) {
            forceLoad();
        }
    }
}
