package com.unseen.fling.data.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.unseen.fling.data.sql.SQLDataHelper;

import static com.unseen.fling.data.provider.FlingContract.Flings;

public class FlingContentProvider extends ContentProvider {

    public static final String AUTHORITY = "com.unseen.fling.data.provider";

    private static final int FLINGS = 10;
    private static final int FLING = 11;
    private static final int FLING_REPORT = 12;

    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        sURIMatcher.addURI(AUTHORITY, Flings.FLING_PATH, FLINGS);
        sURIMatcher.addURI(AUTHORITY, Flings.FLING_PATH + "/#", FLING);
        sURIMatcher.addURI(AUTHORITY, Flings.REPORT_PATH, FLING_REPORT);
    }

    private SQLDataHelper mDbHelper;

    @Override
    public boolean onCreate() {
        mDbHelper = new SQLDataHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        int uriType = sURIMatcher.match(uri);
        Cursor cursor;

        switch (uriType) {
            case FLING:
                cursor = mDbHelper.getReadableDatabase().query(Flings.TABLE_NAME, projection,
                                                               Flings.Columns.ID + " = ?",
                                                               new String[]{String.valueOf(ContentUris.parseId(uri))},
                                                               null, null, null);
                break;
            case FLINGS:
                cursor = mDbHelper.getReadableDatabase().query(Flings.TABLE_NAME, projection, null, null,
                                                               null, null, null);
                break;
            case FLING_REPORT:
                cursor = mDbHelper.getReadableDatabase().rawQuery(
                        "select _id, username, avg(image_size) as image_size, max(width) as width, count(*) as num_post from fling group by user_id",
                        null);
                break;
            default:
                throw new IllegalArgumentException("Unknown query URI: " + uri);
        }

        return cursor;
    }

    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long newId = mDbHelper.getWritableDatabase().insertWithOnConflict(Flings.TABLE_NAME, null, values,
                                                                          SQLiteDatabase.CONFLICT_REPLACE);

        return Uri.withAppendedPath(Flings.CONTENT_URI, String.valueOf(newId));
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FLINGS:
                return mDbHelper.getWritableDatabase().delete(Flings.TABLE_NAME, null, null);
            default:
                throw new IllegalArgumentException("Unknown query URI: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int uriType = sURIMatcher.match(uri);
        switch (uriType) {
            case FLINGS:
                return mDbHelper.getWritableDatabase().updateWithOnConflict(Flings.TABLE_NAME, values, selection, selectionArgs,
                                                                            SQLiteDatabase.CONFLICT_REPLACE);
            default:
                throw new IllegalArgumentException("Unknown query URI: " + uri);
        }
    }
}
