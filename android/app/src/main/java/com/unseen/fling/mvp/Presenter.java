package com.unseen.fling.mvp;

import android.app.Activity;
import com.unseen.fling.app.BaseActivity;

import java.lang.ref.WeakReference;

/**
 * @param <T>
 */
public abstract class Presenter<T> {

    protected T mScene;
    protected WeakReference<BaseActivity> mActivity;

    protected Presenter(T scene) {
        mScene = scene;
    }

    public void initActivity(BaseActivity activity) {
        mActivity = new WeakReference<BaseActivity>(activity);
    }

    public abstract void onStart();
    public abstract void onPause();
    public abstract void onResume();

    public abstract void loadData();

    public Activity getActivity() {
        return mActivity.get();
    }
}
