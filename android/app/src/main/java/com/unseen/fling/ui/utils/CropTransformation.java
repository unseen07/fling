package com.unseen.fling.ui.utils;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import com.squareup.picasso.Transformation;
import com.unseen.fling.data.provider.FlingContract;
import com.unseen.fling.stats.StatsService;

/**
 * Created by Steven on 21/10/2015.
 */
public class CropTransformation implements Transformation {

    private int mFlingId;
    private Context mContext;

    public enum CropType {
        TOP,
        CENTER,
        BOTTOM
    }

    private int mWidth;
    private int mHeight;
    private CropType mCropType = CropType.CENTER;

    public CropTransformation() {
    }

    public CropTransformation(Context context, int flingId, int width, int height) {
        this(context, flingId, width, height, CropType.CENTER);
    }

    public CropTransformation(Context context, int flingId, int width, int height, CropType cropType) {
        mContext = context;
        mFlingId = flingId;
        mWidth = width;
        mHeight = height;
        mCropType = cropType;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        savePhotoStats(mContext, source);
        mWidth = mWidth == 0 ? source.getWidth() : mWidth;
        mHeight = mHeight == 0 ? source.getHeight() : mHeight;

        float scaleX = (float) mWidth / source.getWidth();
        float scaleY = (float) mHeight / source.getHeight();
        float scale = Math.max(scaleX, scaleY);

        float scaledWidth = scale * source.getWidth();
        float scaledHeight = scale * source.getHeight();
        float left = (mWidth - scaledWidth) / 2;
        float top = getTop(scaledHeight);
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        Bitmap bitmap = Bitmap.createBitmap(mWidth, mHeight, source.getConfig());
        Canvas canvas = new Canvas(bitmap);
        canvas.drawBitmap(source, null, targetRect, null);
        source.recycle();

        return bitmap;
    }

    @Override
    public String key() {
        return "CropTransformation(width=" + mWidth + ", height=" + mHeight + ", cropType=" + mCropType
                + ")";
    }

    private float getTop(float scaledHeight) {
        switch (mCropType) {
            case TOP:
                return 0;
            case CENTER:
                return (mHeight - scaledHeight) / 2;
            case BOTTOM:
                return mHeight - scaledHeight;
            default:
                return 0;
        }
    }

    private void savePhotoStats(Context context, Bitmap bitmap) {
        Intent intent = new Intent(context, StatsService.class);
        intent.setAction(StatsService.ACTION_SAVE_IMAGE_INFO);
        intent.putExtra(FlingContract.Flings.Columns.ID, mFlingId);
        intent.putExtra(FlingContract.Flings.Columns.WIDTH, bitmap.getWidth());
        intent.putExtra(FlingContract.Flings.Columns.IMAGE_SIZE, bitmap.getByteCount());
        context.startService(intent);
    }
}
