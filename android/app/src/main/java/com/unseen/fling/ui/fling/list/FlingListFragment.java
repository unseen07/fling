package com.unseen.fling.ui.fling.list;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.*;
import com.unseen.fling.R;
import com.unseen.fling.app.BaseFragment;
import com.unseen.fling.model.Fling;
import com.unseen.fling.ui.utils.UIUtils;

import java.util.List;

public class FlingListFragment extends BaseFragment<FlingListPresenter> implements FlingListScene, FlingListAdapter.FlingItemCallback {

    private RecyclerView mRecyclerView;
    private FlingListAdapter mMovieListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter(new FlingListPresenter(this));
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fling_list, null);
        mRecyclerView = UIUtils.findViewById(rootView, R.id.rv_movies);
        final GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mLoadingView = rootView.findViewById(R.id.loading_view);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_fling_list, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.action_report){
            mPresenter.printReport();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showFlingList(List<Fling> flingList) {
        if (mMovieListAdapter == null) {
            mMovieListAdapter = new FlingListAdapter(flingList);
            mMovieListAdapter.setItemCallback(this);
            mRecyclerView.setAdapter(mMovieListAdapter);
        }
        mMovieListAdapter.notifyDataSetChanged();
        showLoadingView(false);
    }

    @Override
    public void showError(int errorMsg) {
        showErrorMessage(getString(errorMsg));
    }

    @Override
    public void showLoading() {
        showLoadingView(true);
    }

    public static Fragment newInstance() {
        return new FlingListFragment();
    }

    @Override
    public void onFlingClick(Fling fling) {
        // do something here
    }
}
