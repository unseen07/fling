package com.squareup.picasso;

/**
 * Created by Steven on 21/10/2015.
 */
public class PicassoTools {

    public static void clearCache(Picasso p) {
        p.cache.clear();
    }
}
