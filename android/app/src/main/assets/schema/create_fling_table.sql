CREATE TABLE fling(
    _id integer,
    image_id integer,
    user_id integer,
    title VARCHAR(100),
    username VARCHAR(100),
    image_size number,
    width integer
);
CREATE UNIQUE INDEX unique_id on fling (_id);